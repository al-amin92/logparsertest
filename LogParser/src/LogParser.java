import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by al-amin on 11/1/16.
 */


public class LogParser {


    /*

    Running From Command Line:

    java -cp . LogParser 04-sample-log-file.log
    java -cp . LogParser 04-sample-log-file.log --sort
    java -cp . LogParser

    Running From IDE:

    the boolean variable 'sort' determines the output list (true : sorted, false : unsorted)

    */

    public static void main(String[] args) {
        try {
            // Preparing Summary
            boolean sort = false; // sort flag
            Controller controller = new Controller();
            List<LogInfo> list;
            list = controller.generateList(sort, args);

            // Showing Output
            ViewOutput viewOutput = new ViewOutput();
            viewOutput.view(list);

        } catch (IOException e) {
            System.out.println("File Name Not Specified. Please Try Again.");
        }


    }
}
