import java.util.List;

/**
 * Created by al-amin on 11/3/16.
 */


public class ViewOutput {

    public void view(List<LogInfo> list) {


        //Output Formatting

        String col[] = new String[10];
        col[0] = "Time";
        col[1] = "GET / POST Count";
        col[2] = "Unique URI Count";
        col[3] = "Total Response Time";
        System.out.printf("%-18s %-18s %-18s %-20s\n", col[0], col[1], col[2], col[3]);

        for (int i = 0; i < list.size(); i++) {

            int time = list.get(i).time;
            int get = list.get(i).gCount;
            int post = list.get(i).pCount;
            String sTime;
            String getPost;
            getPost = Integer.toString(get) + "/" + Integer.toString(post);
            String responseTime;
            responseTime=list.get(i).responseTime+"ms";

            if (time < 12) {
                if (time == 0)
                    time = 12;
                sTime = Integer.toString(time) + " am - ";
                if (time == 12)
                    sTime += "1 am";
                else if (time == 11)
                    sTime += "12 pm";
                else
                    sTime += Integer.toString(time + 1) + " am";
            } else {
                time -= 12;
                if (time == 0)
                    time = 12;
                sTime = Integer.toString(time) + " pm - ";
                if (time == 12)
                    sTime += "1 pm";
                else if (time == 11)
                    sTime += "12 am";
                else
                    sTime += Integer.toString(time + 1) + " pm";
            }

            //if((get+post)>0) {
            //    System.out.printf("%18s %18s %18d %18dms\n", sTime, getPost, list.get(i).uri, list.get(i).responseTime);
            //}

            System.out.printf("%-18s %-18s %-18d %-18s\n", sTime, getPost, list.get(i).uri,responseTime);
        }
    }
}
