import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by al-amin on 11/2/16.
 */


public class MatchRegex {

    String logTime = new String();
    String getOrPost = new String();
    String uri = new String();
    String responseTime = new String();

    public String getLogTime() {
        return logTime;
    }

    public String getGetOrPost() {
        return getOrPost;
    }

    public String getUri() {
        return uri;
    }

    public String getResponseTime() {
        return responseTime;
    }

    void initStrings() {
        logTime = null;
        getOrPost = null;
        uri = null;
        responseTime = null;
    }

    void match(String line) {

        String pattern = "\\d+-\\d+-\\d+\\s+(\\d+):\\d+:\\d+,\\d+\\s+\\[\\[.*?\\].*?\\]\\s+\\S+\\s+\\S+\\s+\\[.*?\\]\\s+\\w+\\s+-\\s+(URI=(\\[.*?\\]),\\s+(G|P),\\s+time=(\\d+)ms.*)|(.*)";

        initStrings();

        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(line);

        if (m.find()) {
            for (int j = 0; ; j++) {
                if (m.group(j) == null)
                    break;
                //System.out.println(j+": "+m.group(j));
                if (j == 1)
                    logTime = m.group(j);
                if (j == 4)
                    getOrPost = m.group(j);
                if (j == 3)
                    uri = m.group(j);
                if (j == 5)
                    responseTime = m.group(j);
            }
        } else {
            System.out.println("no match");
        }


    }
}
