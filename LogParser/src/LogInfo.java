/**
 * Created by al-amin on 11/2/16.
 */


public class LogInfo implements Comparable<LogInfo> {

    public int time, gPCount, uri, responseTime, gCount, pCount;

    public LogInfo(int time, int gCount, int pCount, int gPCount, int uri, int responseTime) {

        this.time = time;
        this.gCount = gCount;
        this.pCount = pCount;
        this.gPCount = gPCount;
        this.uri = uri;
        this.responseTime = responseTime;
    }

    //sort by summation of get count and post count

    @Override
    public int compareTo(LogInfo o) {
        return -Integer.compare(gPCount, o.gPCount);
    }

    // sort by get count then post count

    //@Override
    //public int compareTo(LogInfo o) {
    //    if(gCount==o.gCount)
    //        return Integer.compare(pCount, o.pCount);
    //    return Integer.compare(gCount, o.gCount);
    //}


}