import java.util.*;

/**
 * Created by al-amin on 11/2/16.
 */


public class GenerateList {
    int SIZE = 26;
    Set<String> mppUri[] = new HashSet[SIZE];
    int totResponseTime[] = new int[SIZE];
    int get[] = new int[SIZE];
    int post[] = new int[SIZE];

    GenerateList() {
        for (int i = 0; i < mppUri.length; i++) {
            mppUri[i] = new HashSet<String>();
        }

        for (int i = 0; i < SIZE; i++) {
            get[i] = 0;
            post[i] = 0;
            totResponseTime[i] = get[i] = post[i] = 0;
        }
    }

    void insertInfo(int logTime, char getOrPost, String uri, int responseTime) {
        mppUri[logTime].add(uri);
        totResponseTime[logTime] += responseTime;

        if (getOrPost == 'G')
            get[logTime]++;
        else
            post[logTime]++;
    }


    int getGetCount(int hour) {
        return get[hour];
    }

    int getPostCount(int hour) {
        return post[hour];
    }

    int getUniqueUriCount(int hour) {
        return mppUri[hour].size();
    }

    int getTotalResponseTime(int hour) {
        return totResponseTime[hour];
    }
}

