import java.io.*;
import java.util.ArrayList;

/**
 * Created by al-amin on 11/2/16.
 */


class ReadLogFile {
    ArrayList<String> getLogs(String fileName) throws IOException {

        ArrayList<String> lines = new ArrayList<String>();
        FileReader fileReader = new FileReader(fileName);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String str;

        while ((str = bufferedReader.readLine()) != null) {
            lines.add(str);
        }

        return lines;
    }
}
